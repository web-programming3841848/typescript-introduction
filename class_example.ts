class Person{
    public constructor(private readonly name:string){

    }

    public getName(): string{
        return this.name;
    }
}
const person = new Person("Jane");
console.log(person);
console.log(person.getName());

interface Shape{
    getArea:() => number;
}

class Rectrangle1 implements Shape {
    public constructor(protected readonly width:number,protected readonly height:number){}
    public getArea():number{
        return this.width*this.height;
    }
}

const rect:Rectrangle1 = new Rectrangle1(50,10);
console.log(rect);
console.log(rect.getArea());

class Square  extends Rectrangle1{
    public constructor(width: number){
        super(width,width);
    }
    public toString(): string{
        return `Square [${this.width}]`;
    }
}
const square: Square = new Square(10);
console.log(square);
console.log(square.getArea());
console.log(square.toString());

abstract class Polygon {
    public abstract getArea(): number;

    public toString(): string{
        return `Polygon [area = ${this.getArea()}]`
    }
}
class Rectrangle2 extends Polygon{
    public constructor(protected readonly width:number,protected readonly height:number){
        super();
    }
    public getArea(): number {
        throw this.width*this.height;
    }

}

const rect2:Rectrangle2 = new Rectrangle2(50,10);
console.log(rect2);
console.log(rect2.toString());