const names: String[] = [];
names.push("Dylan");
names.push("Tanipong");
console.log(names[0])
console.log(names[1])
console.log(names)
console.log(names.length)

for(let i = 0;i < names.length;i++){
    console.log(names[i]);
}

console.log();

for(let i in names){
    console.log(names[i]);
}

console.log();

names.forEach(function(name){
    console.log(name);
});