type CarYear = number;
type CarType = string;
type CarModel = string;

type Car = {
    year: CarYear,
    Type: CarType,
    Model: CarModel,
}

const carYear: CarYear = 2009;
const carType: CarType = "Toyota";
const carModel: CarModel = "Coralla";

const car1:Car = {
    year: carYear,
    Type: carType,
    Model: carModel
}
console.log(car1);