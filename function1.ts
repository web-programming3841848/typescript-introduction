function getTime(): number {
    return new Date().getTime();
}
console.log(getTime());

function getFormattedTime() {
    const currentDate = new Date();
    const formattedDateTime = currentDate.toLocaleString();

    return formattedDateTime;
}

console.log(getFormattedTime());

function printHello() : void{
    console.log("Hello");
}
printHello();

function multiply(a: number,b: number):number{
    return a*b;
}
console.log(multiply(5,5));
console.log(multiply(7,9));

function add(a: number,b: number,c?: number):number{
    return a + b + (c||0);
}
console.log(add(1,2,7));
console.log(add(6,4));

function pow(value: number,exponent: number):number{
    return value ** exponent;
}
console.log(pow(6,4));
console.log(pow(10,3));

function divide({dividend,devisor}: {dividend: number,devisor: number}): number{
    return dividend/devisor;
}

console.log(divide({dividend: 100,devisor: 10}));

function add2(a: number,b: number,...rest: number[]){
    return a + b + rest.reduce((p,c) => p + c , 0); 
}

console.log(add2(0,1,2,3,4));

type Negate = (value: number) => number;
const negateFunction: Negate =  (value: number) => value*-1;
const negateFunction2: Negate =  function(value:number): number{
    return value*-1;
};
console.log(negateFunction(5));
console.log(negateFunction2(5));